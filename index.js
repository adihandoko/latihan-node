var http = require('http');
var url = require('url');
var fs = require('fs');
var path = require('path');

var index='index.html';
var assets='assets';

http.createServer(function (req, res) {
    var q = url.parse(req.url, true);
    if (req.url.match("\.html") || req.url === "/admin" || req.url === "/") {
        q.pathname=index;
      var sisi='client';
      if (req.url.match("/admin") ) {
          sisi='admin';
      }

      var filename = "./"+q.pathname;
      if (sisi !='') {
        var filename = "./"+sisi+'/'+q.pathname;
      }
      console.log(filename);
      fs.readFile(filename, function(err, data) {
        if (err) {
          res.writeHead(404, {'Content-Type': 'text/html'});
          return res.end("404 Not Found");
        }  
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.write(data);
        return res.end();
      });

    } else if (!req.url.match("\.html") ) {
      if (req.url.match("\.css")) {
        var filename = "./"+assets+'/'+q.pathname;
        var css_path=path.join(__dirname,assets,req.url);
        var filesystem=fs.createReadStream(css_path,"UTF-8");
        res.writeHead(200, {'Content-Type': 'text/css'});
        filesystem.pipe(res);
    }else if (req.url.match("\.js")) {
        var css_path=path.join(__dirname,assets,req.url);
        var filesystem=fs.createReadStream(css_path,"UTF-8");
        res.writeHead(200, {'Content-Type': 'text/javascript'});
        filesystem.pipe(res);
    }else if (req.url.match("\.png")) {
        var png_path=path.join(__dirname,assets,req.url);
        var filesystem=fs.createReadStream(png_path);
        res.writeHead(200, {'Content-Type': 'image/png'});
        filesystem.pipe(res);
    }else if (req.url.match("\.jpg")) {
        var png_path=path.join(__dirname,assets,req.url);
        var filesystem=fs.createReadStream(png_path);
        res.writeHead(200, {'Content-Type': 'image/jpg'});
        filesystem.pipe(res);
    }else{
          res.writeHead(404, {'Content-Type': 'text/html'});
          return res.end("404 Not Found");
    }
    }
    
}).listen(8080);

	console.log('listen 8080');